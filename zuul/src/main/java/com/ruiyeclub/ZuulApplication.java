package com.ruiyeclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author Ray。
 * @create 2020-02-28 14:59
 */
@EnableZuulProxy //包含了@EnableZuulServer，这只该网关的启动类
//帮助SpringBoot应用将所有符合条件的@configuration配置加载到当前SpringBoot创建并使用的IOC容器中
@EnableAutoConfiguration
public class ZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class,args);
    }
}