package com.ruiyeclub.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ray。
 * @create 2020-02-27 19:29
 */
@Data
@AllArgsConstructor //添加有参构造函数
@NoArgsConstructor //添加无参构造函数
public class Student {
    private long id;
    private String name;
    private int age;
}