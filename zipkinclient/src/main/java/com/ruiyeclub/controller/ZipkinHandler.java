package com.ruiyeclub.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ray。
 * @create 2020-03-02 18:52
 */
@RestController
@RequestMapping("/zipkin")
public class ZipkinHandler {
    @Value("{server.port}")
    private String port;

    @GetMapping("/index")
    public String index(){
        return this.port;
    }
}