package com.ruiyeclub.repository;

import com.ruiyeclub.entity.Student;

import java.util.Collection;

/**
 * @author Raychen
 * @date 2020/2/27 19:31
 */
public interface StudentRepository {

    public Collection<Student> findAll();
    public Student findById(long id);
    public void saveOrUpdate(Student student);
    public void deleteById(long id);
}
