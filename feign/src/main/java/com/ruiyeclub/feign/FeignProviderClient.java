package com.ruiyeclub.feign;

import com.ruiyeclub.entity.Student;
import com.ruiyeclub.feign.impl.FeignError;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

/**
 * @author Ray。
 * @create 2020-02-28 21:21
 */
//访问注册中心叫provider的服务
//fallback降级处理，找得到去provider，找不到去FeignError
@FeignClient(value = "provider",fallback = FeignError.class)
public interface FeignProviderClient {
    @GetMapping("/student/findAll")
    public Collection<Student> findAll();

    @GetMapping("/student/index")
    public String index();
}