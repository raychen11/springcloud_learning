package com.ruiyeclub.feign.impl;

import com.ruiyeclub.entity.Student;
import com.ruiyeclub.feign.FeignProviderClient;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * @author Ray。
 * @create 2020-02-28 23:03
 * 创建FeignProviderClient接口的实现类FeignError，定义容错处理逻辑，通过@component注解将
 * FeignError实例注入IOC容器中。
 */
@Component
public class FeignError implements FeignProviderClient {

    @Override
    public Collection<Student> findAll() {
        return null;
    }

    @Override
    public String index() {
        return "服务器维护中...";
    }
}