##负载均衡Feign
·什么是Feign？
与Ribbon意义，Feign也是由Netflix提供的，Feign是一个声明式，模板版话的Web Service客户端，它简化了开发者编写Web服务客户端的操作，开发者可以通过简单的接口和注解来调用HTTP API，Spring Cloud Feign，它整合了Ribbon和Hystrix，具有可插拔、基于注解、负载均衡、服务熔断等一系列便捷功能。

相较于Ribbon+RestTemplate的方式，Feign大大简化了代码的开发，Feign支持多种注解，包括Feign注解、JAX-RS注解、Spring MVC注解等，Spring Cloud对Feign进行了优化，整合了Ribbon和Eureka，从而让Feign的使用更加方便。

·Ribbon和Feign的区别
1、Feign是一个声明式的Web Service客户端。
2、支持Feign注解、Spring MVC注解、JAX-RS注解。
3、Feign基于Ribbon实现，使用起来更加简单。
4、Feign集成了Hystrix、具备服务熔断的功能。

·服务熔断，application.yml添加熔断机制。
#开启熔断器
feign:
  hystrix:
    enabled: true
这里就表示是否开启熔断器，默认不开启。
·创建FeignProviderClient接口的实现类FeignError，定义容错处理机制，通过@component注解将FeignError实例注入IOC容器中。 
·在FeignProviderClient定义处通过@FeignClient的fallback属性设置映射。